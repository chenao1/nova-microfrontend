import hypernova from 'hypernova/server'
import { renderSvelte } from 'hypernova-svelte'
import express from 'express'
import path from 'path'

import Example from './components/Example.svelte'

hypernova({
  devMode: process.env.NODE_ENV !== 'production',
  getComponent (name) {
    if (name === 'SvelteExample') {
      return renderSvelte(name, Example)
    }

    return null
  },
  port: process.env.PORT || 3001,

  createApplication () {
    const app = express()

    app.use('/public', express.static(path.join(process.cwd(), 'dist')))

    return app
  }
})
