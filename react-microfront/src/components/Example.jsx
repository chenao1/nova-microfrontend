import React from 'react';
import PropTypes from 'prop-types';

const Example = ({ title, height, width }) => (
  <div>
    <h1>{ title }</h1>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/245px-React.svg.png" height={ height } width={ width } />
    <p>it just a react project</p>
  </div>
);

Example.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Example;
