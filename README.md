# Microfrontends with nuxt

## Nuxt aplication

Change directory to `nuxt-microfront`

```
cd nuxt-microfront/
```

Install nuxt depedencies
```
npm install
```

Run nuxt application
```
npm run dev
```

## Start whatever microfront.

Change directory to microfront which you want to start (i.e. vue-microfront).

```
cd vue-microfront
```
Install all dependencies
```
npm install
```
Start application
```
npm run dev
```
